import java.text.NumberFormat;

public class NonMemberCustomer extends Customer
{
	//nonmembers have to pay an entrance fee
	private double visitFee;

	public NonMemberCustomer(String fName, String lName, double purchAmount, int purchYear, int purchMonth, int purchDate, double vFee)
	{
		//assigns parameters to protected instance fields in class customer
		super(fName, lName, purchAmount, purchYear, purchMonth, purchDate);
		visitFee = vFee;
	}

	//payment amount is the purchased amount plus the visit fee
	public void computePaymentAmount()
	{
		paymentAmount = super.purchasedAmount + visitFee;
	}

	//overriding toString method in the Customer class
	public String toString()
	{
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		String aboutInfo = "NonMember Customer:" + super.toString() +"\nVisit Fee:\t\t" + nf.format(visitFee);
		return aboutInfo;
	}

}