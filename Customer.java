import java.text.NumberFormat;

//superclass for Member and NonMember
public abstract class Customer
{
	protected String firstName;
	protected String lastName;
	protected String studentId;
	protected double purchasedAmount;
	protected int purchasedYear;
	protected int purchasedMonth;
	protected int purchasedDate;
	protected double paymentAmount;

	public Customer(String fName, String lName, double purchAmount, int purchYear, int purchMonth, int purchDate)
	{
		//essential variables for the member and non members
		firstName = fName;;
		lastName = lName;
		purchasedAmount = purchAmount;
		purchasedYear = purchYear;
		purchasedMonth = purchMonth;
		purchasedDate = purchDate;

	}

	//returns the original price amount for the product
	public double getPurchasedAmount()  //int amount = 16;
	{
		return purchasedAmount;
	}

	//computes total payment, abstract method has to be implemented
	public abstract void computePaymentAmount();

	//standard toString method
	public String toString()
	{
		NumberFormat nf = NumberFormat.getCurrencyInstance();

		String aboutInfo = "\nFirst name:\t\t" + firstName + "\n" +
								"Last name:\t\t" + lastName + "\n" +
								"Purchased Amount:\t" + nf.format(purchasedAmount) + "\n" +
								"Purchased Date:\t\t" + purchasedMonth + "/" + purchasedDate + "/" + purchasedYear + "\n" +
								"Payment Amount:\t\t" + nf.format(paymentAmount);

		return aboutInfo;
	}

}