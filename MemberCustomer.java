
public class MemberCustomer extends Customer
{
	//extra instance variable for MemberCustomer specifies point Customer has collected
	private int pointsCollected;

	//pulls variable from super class with super constructor
	public MemberCustomer(String fName, String lName, double purchAmount, int purchYear, int purchMonth, int purchDate, int ptsCollected)
	{
		super(fName, lName, purchAmount, purchYear, purchMonth, purchDate);
		pointsCollected = ptsCollected;

	}

	//determines the discount given to the member based on whether the cusomer has over 100 points
	public void computePaymentAmount()
	{
		if(pointsCollected > 100)
		{
			super.paymentAmount = super.purchasedAmount * 0.8;
		}
		else
		{
			super.paymentAmount = super.purchasedAmount * 0.9;
		}

		pointsCollected += (int) (super.purchasedAmount * 0.01);

	}

	//overrides Customer superclass toString method
	public String toString()
	{
		String aboutInfo = "Member Customer:" + super.toString()+ "\nCollected Points:\t" + pointsCollected;
		return aboutInfo;
	}

}


