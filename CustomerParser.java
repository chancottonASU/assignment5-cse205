

public class CustomerParser
{
	//blank constructor
	public CustomerParser()
	{
	}

	public static Customer parseStringToCustomer(String lineToParse)
	{
		//splits strings with each "//" returns String[] with
		String[] customerInfo = lineToParse.split("/");

		String fName = customerInfo[1];
		String lName = customerInfo[2];
		double purchAmount = Double.parseDouble(customerInfo[3]);
		int purchYear = Integer.parseInt(customerInfo[4]);
		int purchMonth = Integer.parseInt(customerInfo[5]);
		int purchDate = Integer.parseInt(customerInfo[6]);

		//pointsCollected and visitFee are both inititalized to the same value
		//if customer is member, pointsCollected will be initialized into the Member constructor
		//otherwise visitFee will be added into the NonMember constructor



		String customerType = customerInfo[0];

		//if a member, returns instance of member
		if(customerType.equalsIgnoreCase("member"))
		{
			int pointsCollected = Integer.parseInt(customerInfo[7]);
			return new MemberCustomer(fName, lName, purchAmount, purchYear, purchMonth, purchDate, pointsCollected);
		}

		//else if, returns an instance  of nonmember
		else if(customerType.equalsIgnoreCase("nonmember"))
		{
			double visitFee = Double.parseDouble(customerInfo[7]);
			return new NonMemberCustomer(fName, lName, purchAmount, purchYear, purchMonth, purchDate, visitFee);
		}

		else
		{
			return null;
		}


	}


}